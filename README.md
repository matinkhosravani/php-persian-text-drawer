gd-text
=======

###Basic usage example
```php
<?php
require __DIR__.'/../vendor/autoload.php';

use GDText\Box;
use GDText\Color;

$im = imagecreatetruecolor(500, 500);
$backgroundColor = imagecolorallocate($im, 0, 18, 64);
imagefill($im, 0, 0, $backgroundColor);

    $box = new Box($im);
    $box->setFontFace(storage_path() . '/app/vazir.ttf');
    $box->setFontColor(new Color(0, 0, 0));
    $box->setTextShadow(new Color(0, 0, 0, 50), 2, 2);
    $box->setFontSize(14);
    $box->setStrokeColor(new Color(255, 75, 140)); // Set stroke color
    $box->setStrokeSize(2); // Stroke size in pixels
    $box->setBox(0, 20, $blurredImage->getWidth(), 50);
    $box->setTextAlign('center', 'top');
    $text = "فقط با 165 هزار تومان 1400 گیگ اینترنت پر سرعت آسیاتک بگیر ! ویژه مشترکین جدید";
    $box->draw($decorator->decorate($text, true, false));


header("Content-type: image/png");
imagepng($im);
```


Check out here for more information and examples : https://github.com/stil/gd-text
